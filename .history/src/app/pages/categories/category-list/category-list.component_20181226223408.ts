import { Component, OnInit } from '@angular/core';

import { Category } from '../shared/category.model';
import { CategoryService } from '../shared/category.service';

// Componente de Mensagens
import { SweetAlertService } from 'ng2-sweetalert2'; 

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css'],
  providers: [SweetAlertService]
})
export class CategoryListComponent implements OnInit {
  categories: Category[] = [];

  static get parameters() {
    return [[SweetAlertService]];
  }

  constructor(private categoryService: CategoryService, swal) {
    this.swalService = swal;
  }

  ngOnInit() {
    this.categoryService.getAll().subscribe(
      categories => {
        this.categories = categories;
        console.log(categories);
      },
      error => alert('Erro ao carregar a lista')
    );
  }

  deleteCategory(category) {
    const mustDelete = confirm('Deseja realmente excluir ?');

    if (mustDelete) {
      this.categoryService.delete(category.id).subscribe(
        () => {
          this.categories = this.categories.filter(
            element => element !== category
          );
          this.showMessage('Registro excluído com sucesso!');
        },
        () => alert('Erro ao tentar excluir')
      );
    }
  }

  // PRIVATE METHODS
  private showMessage(msg: string) {
    // swal('Mensagem do Sistema', msg, 'success');
  }
}
