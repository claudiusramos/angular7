import { Component } from '@angular/core';

import swal, { SweetAlertOptions} from 'sweetalert2';
import { SwalComponent } from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'finansys';

  public alertOption: SweetAlertOptions = {};

  constructor() {
    this.alertOption = {
      title: 'Are you sure?',
      text: 'Do you want to save changes',
      cancelButtonColor: '#d33',
      showCancelButton: true,
      cancelButtonText: 'No! Review',
      confirmButtonColor: '#3085d6',
      confirmButtonText: 'Yes, Save progress',
      showLoaderOnConfirm: true,
      focusCancel: true,
      preConfirm: () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log( 'Doing async operation');
        resolve();
      }, 5000);
    });
  },
  allowOutsideClick: () => !swal.isLoading()
    };
  }
}
