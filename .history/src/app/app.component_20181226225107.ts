import { Component } from '@angular/core';

import swal, { SweetAlertOptions} from 'sweetalert2';
import { SwalComponent } from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'finansys';

  public alertOption: SweetAlertOptions = {};
}
