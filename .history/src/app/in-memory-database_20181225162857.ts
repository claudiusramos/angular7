import { InMemoryDbService } from 'angular-in-memory-web-api';

import { Category } from './pages/categories/shared/category.model'
export class InMemoryDatabase implements InMemoryDbService {
 createDb() {
     const categories: Category[] = [
       { id: 1, name: 'Moradia', description: 'Pagamentos de Contas da Casa'},
       { id: 2, name: 'Saúde', description: 'Planos de Saúde'},
       { id: 3, name: 'Lazer', description: 'Pagamentos de Contas da Casa'},
       { id: 4, name: 'Salário', description: 'Pagamentos de Contas da Casa'},
       { id: 5, name: 'Freelas', description: 'Pagamentos de Contas da Casa'}
     ];

     return { categories };
 }
}
