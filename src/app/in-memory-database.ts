import { InMemoryDbService } from 'angular-in-memory-web-api';

import { Category } from './pages/categories/shared/category.model';
import { Entry } from './pages/entries/shared/entry.model';

export class InMemoryDatabase implements InMemoryDbService {
  createDb() {
    const categories: Category[] = [
      { id: 1, name: 'Moradia', description: 'Pagamentos de Contas da Casa' },
      { id: 2, name: 'Saúde', description: 'Planos de Saúde' },
      { id: 3, name: 'Lazer', description: 'Passeios com as crianças' },
      { id: 4, name: 'Salário', description: 'Dindin' },
      { id: 5, name: 'Freelas', description: 'Serviços extras' }
    ];

    const entries: Entry[] = [
      {
        id: 1,
        name: 'Gás de cozinha',
        categoryId: categories[0].id,
        category: categories[0],
        paid: true,
        date: '14/10/2018',
        amount: '70,80',
        type: 'expense',
        description: 'Qualquer descrição'
      } as Entry,
      {
        id: 1,
        name: 'Gás de cozinha',
        categoryId: categories[0].id,
        category: categories[0],
        paid: true,
        date: '14/10/2018',
        amount: '900,80',
        type: 'revenue',
        description: 'Qualquer descrição'
      } as Entry,
      {
        id: 1,
        name: 'Gás de cozinha',
        categoryId: categories[0].id,
        category: categories[0],
        paid: true,
        date: '14/10/2018',
        amount: '70,80',
        type: 'expense',
        description: 'Qualquer descrição'
      } as Entry,
      {
        id: 1,
        name: 'Gás de cozinha',
        categoryId: categories[0].id,
        category: categories[0],
        paid: false,
        date: '14/10/2018',
        amount: '70,80',
        type: 'expense',
        description: 'Qualquer descrição'
      } as Entry
    ];

    return { categories, entries };
  }
}
