import { Component, OnInit } from '@angular/core';

import { Entry } from '../shared/entry.model';
import { EntryService } from '../shared/entry.service';

// Componente de Mensagens
 import swal from 'sweetalert2';

@Component({
  selector: 'app-entry-list',
  templateUrl: './entry-list.component.html',
  styleUrls: ['./entry-list.component.css']
})
export class EntryListComponent implements OnInit {
  entries: Entry[] = [];

  constructor(private entryService: EntryService) {}

  ngOnInit() {
    this.entryService.getAll().subscribe(
      entries => {
        this.entries = entries;
      },
      error => alert('Erro ao carregar a lista')
    );
  }

  deleteEntry(entry) {
    const mustDelete = confirm('Deseja realmente excluir ?');

    if (mustDelete) {
      this.entryService.delete(entry.id).subscribe(
        () => {
          this.entries = this.entries.filter(
            element => element !== entry
          );
          this.showMessage('Registro excluído com sucesso!');
        },
        () => alert('Erro ao tentar excluir')
      );
    }
  }

  // PRIVATE METHODS
  private showMessage(msg: string) {
     swal('Mensagem do Sistema', msg, 'success');
  }

  private mensagemCustomizada() {
    swal({
      title: '<strong>HTML <u>example</u></strong>',
      type: 'info',
      html:
        'You can use <b>bold text</b>, ' +
        '<a href="//github.com">links</a> ' +
        'and other HTML tags',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Great!',
      confirmButtonAriaLabel: 'Thumbs up, great!',
      cancelButtonText:
        '<i class="fa fa-thumbs-down"></i>',
      cancelButtonAriaLabel: 'Thumbs down',
    });
  }

}
